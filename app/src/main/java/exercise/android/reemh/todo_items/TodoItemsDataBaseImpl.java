package exercise.android.reemh.todo_items;

import java.util.List;

// TODO: implement!
public class TodoItemsDataBaseImpl implements TodoItemsDataBase {
  @Override
  public List<TodoItem> getCurrentItems() { return null; }

  @Override
  public void addNewInProgressItem(String description) { }

  @Override
  public void markItemDone(TodoItem item) { }

  @Override
  public void markItemInProgress(TodoItem item) {}

  @Override
  public void deleteItem(TodoItem item) { }
}
